from flask import Flask
from config import Config
from flask_pymongo import PyMongo

# Creating a Flask object
app = Flask(__name__)

# Setting app config for everything together using the Config class
app.config.from_object(Config)
# hence no need to individually apply configuration like below.
# app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"

# Create mongo object from Flask-PyMongo libarary
mongo = PyMongo(app)


# Importing routes/views separately to keep code clean and extensible
from app.scripts import views

# Run the Flask app, debug=True makes reload server on file saves automatic. 
# print (Config.APP_DEBUG)
app.run()
# app.run(debug=Config.FLASK_DEBUG)
