from flask import render_template, url_for, request, redirect, flash
from app import app, mongo
from app.scripts.forms import EnterCid
from app.scripts.tables import AdminTable
from app.scripts.account import Account
from bson.json_util import dumps, loads # very useful in converting Cursor object to serializable/JSON object
import json


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/home2')
def home2():
    # Test function
    return render_template('home2.html')

@app.route('/activity')
def activity():
    # This should only be limited to views and rendering and passing the correct tables. 
    # All the logic should happen in a different module
    # So, essentially, when this view is called, it should only fetch tables to pass to rendering maching.
    return render_template('activity.html')

@app.route('/essentials')
def essentials():
    return render_template('essentials.html')

@app.route('/lastResponse')
def lastResponse():
    return render_template('lastResponse.html', lr=lr)

@app.route('/admin', methods=['GET', 'POST'])
def admin():
    form = EnterCid()

    table=AdminTable(json.loads(dumps(mongo.db.accounts.find().sort('_id',-1))))

    if form.validate_on_submit():
        flash('Validation requested for user {}'.format(form.api_email.data))
        # return /admin on valid submit, so the form clears and HTTP clears the state of the browser

        # If html validation is passed, then continue to validate credentials via Kentik v5 API.
        # This can be a part of post_validation (not implemented as a part of built in method today)
        # Call another method to return a dictionary object to append to list of collections
        # Addition to the list of collections should check for duplicates.
        # If validation fails, then record the error and display in /lastResponse
        # Bonus points: if you clear the class object

        # Create an objext of class Account, which will have all attributes (and associated callable methods)
        a = Account()

        # Assign textbox/stringfeild data from HTML (rendered by flask_wtf via wtforms) into python objects
        # This is where HTML-to-Python happens
        a.api_email=form.api_email.data
        a.api_token=form.api_token.data
        a.golden=form.golden.data
        if form.c_name.data is not "": a.c_name=form.c_name.data  # Since DataRequired() is False, check if it exists. 

        a.validate()

        # Print all properties of object (a) of a class. 
        #print('\n '.join("%s: %s" % item for item in vars(a).items()))

        if a.valid:
            flash('Found valid user as {} under CID:{}'.format(a.api_user,a.cid))
            #print ("Adding found CID {} in {} Database Collections".format(a.cid,mongo.db.cx))
            message = a.register(mongo)
            flash(message)
            #    flash('Registration Complete for {}'.format(a.cid))
        else: flash('Could not Validate {}'.format(a.api_email))
        return redirect('/admin')
    return render_template('admin.html',title="Admin",form=form, table=table)