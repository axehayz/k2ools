# import things
from flask_table import Table, Col, BoolNaCol, BoolCol

# Declare your table
class AdminTable(Table):
    _id = Col('_id',show=False)
    c_name = Col('Friendly Name')
    cid = Col('CID')
    api_email = Col('api_email')
    api_token = Col('api_token',show=False)
    api_user = Col('api_user')
    valid = BoolCol('Valid',show=False)
    golden = BoolCol('Golden')
    
    no_items = "Collections Empty"
    border = True

    #classes = 'cinereousTable'
    classes = ["cinerousTable",] # keep the , as otherwise the classes function from flask-tables will split(' ') the string.




# # Get some objects
# class Item(object):
#     def __init__(self, name, description):
#         self.name = name
#         self.description = description
# items = [Item('Name1', 'Description1'),
#          Item('Name2', 'Description2'),
#          Item('Name3', 'Description3')]
# # Or, equivalently, some dicts
# items = [dict(name='Name1', description='Description1'),
#          dict(name='Name2', description='Description2'),
#          dict(name='Name3', description='Description3')]

# # Or, more likely, load items from your database with something like
# items = ItemModel.query.all()

# # Populate the table
# table = ItemTable(items)

# # Print the html
# print(table.__html__())
# # or just {{ table }} from within a Jinja template