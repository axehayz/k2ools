import json
from app.scripts.utils import query

# from bson.json_util import dumps  ##keeping Account class clean to only consist of methods with no heavy library dependencies 
# so, dumps, Flask, mongo is not imported; instead are passed in as objects.

class Account:
    """Every entry is an attribute to the object of class Account"""
    def __init__(self):
        # Every Account will have these basic attributes
        self.api_email = None
        self.api_token = None
        self.api_user = None
        self.c_name = None
        self.golden = None
        self.cid = int()
        self.valid = False

        # Every Account will have these instantaneous attributes
        self.sites = []
        self.devices = []
        self.users = []
    
    def validate(self):
        """Validate credentials by attempting Login"""
        r, err = query(self,endpoint="users")
        if err is None:
            # Find the user
            for user in r.json()['users']:
                if user['user_email']==self.api_email:
                    # User is found. Set class object attributes and mark self.valid as True
                    self.cid = user['company_id']
                    self.api_user = user['user_full_name']
                    self.valid = True
                    break
            #print (type(r.json()['users']))
            #print ((json.dumps(r.json(),indent=4)))
    
    def register(self, mongo):
        """Register a new CID to collections. Also, implements logic for duplicate entries"""
        def replace_if_duplicate():
            #print (mongo.db.accounts.insertOne({'dict':'key'}))
            # b = '4323'
            # self.cid = b
            match = mongo.db.accounts.find_one({'cid':self.cid}) # findOne doesnt work. use find_one. either of them return a dictionary. find() returns callable object.
            #print (type(match))
            #print ((match))

            if match:
                #print (type(match['valid']))
                #print ((match['valid']))
                ### The reason validity match below is implemented is because of future [TODO] implement validity false runners. 
                if match['valid']:
                    message = ("A valid entry for CID: {} already exists as {} user API".format(match['cid'],match['api_user']))
                    #print (message)
                    return          # code hack. This if clause runs for duplicate entries, and the below if not replace_if_duplicate() will never get run. Hence return empty. 
                else:
                    mongo.db.accounts.replace_one(match,{
                        '_id':match['_id'],'cid':match['cid'],
                        'api_email':self.api_email,'api_token':self.api_token,'api_user':self.api_user,'c_name':self.c_name,
                        'valid':self.valid,'golden':self.golden
                        })
                    message = "seems valid = false. This should be a NULL case in production. In development, this can occur. Check source code"
                    #print (message)
                return True
            return False           # executed if match = False
        if not replace_if_duplicate():
            # then simply add the new entry
            # Code trick, only self.valid=True gets inserted in the database. No condition today where self.valid=False is inserted in DB. 
            mongo.db.accounts.insert_one({
                        'cid':self.cid,'api_email':self.api_email,'api_token':self.api_token,'api_user':self.api_user,'c_name':self.c_name,
                        'valid':self.valid,'golden':self.golden
                        })
            message = "CID added to Account Collections"
        return message
