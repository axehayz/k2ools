#!/usr/bin/env python
# forms.py - Define all the forms the web app will host

# Importing Flask's WTF (What the Forms) extension using common nomenclature flask_wtf <flask_<extension>>
from flask_wtf import FlaskForm
# Importing fields and validators directly from wtforms library as flask_wtf extension lib does not provide customization
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class EnterCid(FlaskForm):
    api_email = StringField('api_email:', validators=[DataRequired()])
    api_token = StringField('api_token:', validators=[DataRequired()])
    c_name = StringField('Friendly Name: ')
    golden = BooleanField('Golden')
    submit = SubmitField('Enter')
