import requests 

def query(self,cluster="", endpoint="users", method="GET", data=""):
    url="https://{}api.kentik.com/api/v5/{}".format(cluster,endpoint)
    headers = {
    "Content-Type": "application/json",
    "X-CH-Auth-API-Token": self.api_token,
    "X-CH-Auth-Email": self.api_email}

    r = None
    error_msg = None
    try:
        if self.api_token and self.api_email:
            if method=="GET":
                r = requests.get(url,headers=headers, stream=True)
                # print (r.text)
                r.raise_for_status()
            elif method=="POST":
                if data!="":
                    r = requests.post(url,headers=headers,data=json.dumps(data), stream=True)
                    r.raise_for_status()
                else:
                    print ("[Err] POST method with empty data")
        else:
            print ("[Err] API token or email not supplied")
    except requests.exceptions.HTTPError as err:                # catch from r.raise_for_status call above.
        error_msg=err
        if err.response.status_code == 401: 
            print('[Err]: Invalid Credentials. Please login again.'); 
        elif err.response.status_code == 403: 
            print('[Err]: IP Unauthorized.'); 
            print('Raw connection socket IP: {}'.format(err.response.raw._connection.sock.getpeername()))
        else: 
            pass 
    except requests.exceptions.ConnectionError as err:          # Catch DNS failures, refused connections. 
        error_msg=err
    except requests.exceptions.RequestException as err:         # Bail
        error_msg=err
    finally:
        if error_msg is not None:
            # Insert a Flash Message
            print (error_msg)
        #print (r)
        return r, error_msg
        # def storeLastResponse():
        #     nonlocal r
        #     nonlocal err
        #     r=r
        #     err=err
        #     lr.update(r=r,err=err)
        #     print (lr)
        # storeLastResponse(r,err)