#!/usr/bin/env python
# config.py - to protect flask-wtf (forms) against CSRF attacks. 
# Configure a SECRET_KEY that will be an attribute of the config object of the Config class.
# Config class can be then extended to contain methods or attributes that can be pulled into the application.

import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'qw3rt^y-saved-the-world'
    # Set environment variables for any configuration variables.

    FLASK_DEBUG = os.environ.get('APP_DEBUG') or False

    # Database configuration
    MONGO_URI = os.environ.get('MONGO_URI') or 'mongodb://localhost:27017/test'