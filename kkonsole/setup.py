from setuptools import setup, find_packages

setup(
    name='kkonsole',
    version='1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Flask',
        'flask_wtf',
        'requests',
        'flask-pymongo',
        'flask-table',
    ],
    # entry_points='''
    #     [console_scripts]
    #     kkonsole=kkonsole:kkonsole
    #     kperform=kperform.kperform:kperform
    #     klookup=klookup.klookup:klookup
    #     kstats=klookup.klookup:stats
    # ''',
)